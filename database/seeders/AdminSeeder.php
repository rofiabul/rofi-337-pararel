<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Suppo\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([


            'name' => 'admin1',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('qwe123')
            ]);
    }
}
